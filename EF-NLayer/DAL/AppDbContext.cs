﻿using Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;

namespace DAL
{
    public class AppDbContext : DbContext
    {
        public DbSet<Person> People { get; set; }
        // geen DbSet-prop voor owned-type(s) voorzien
        // anders toch aparte tabel voor owned-type (~table-splitting)
        // en is het relationeel terug 0..1-op-1
        //public DbSet<Address> Addresses { get; set; }

        public AppDbContext()
        {
            this.Database.EnsureDeleted();
            if (this.Database.EnsureCreated())
                Seed();
        }

        private void Seed()
        {
            Person p1 = new Person() {Name="Kenneth", Age = 41};
            Person p2 = new Person() {Name="Carmen", Age = 23};
            Person p3 = new Person() {Name="Stijn", Age = 27};

            People.Add(p1);
            People.Add(p2);
            People.Add(p3);

            this.SaveChanges();

            this.ChangeTracker.Clear();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite("Data Source=myappdb.sqlite");
                optionsBuilder.LogTo(msg => System.Diagnostics.Debug.WriteLine(msg), LogLevel.Information);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Owned<Address>();

            //modelBuilder.Entity<Person>().Property<int?>("FK_PersonId");
            /*modelBuilder.Entity<Person>()
                .HasOne(person => person.Address)
                .WithOne(address => address.Person)
                .HasForeignKey<Address>("FK_PersonId")
                .IsRequired(true);*/
            modelBuilder.Entity<Person>().OwnsOne(person => person.Address);
            
            modelBuilder.Entity<StudentCourse>()
                .HasOne(pc => pc.Student)
                .WithMany(p => p.Courses)
                .HasForeignKey("FK_StudentId")
                .IsRequired(true);
            modelBuilder.Entity<StudentCourse>()
                .HasOne(pc => pc.Course)
                .WithMany(c => c.Students)
                .HasForeignKey("FK_CourseId")
                .IsRequired(true);
            modelBuilder.Entity<StudentCourse>()
                .HasKey(new string[] { "FK_StudentId", "FK_CourseId" });

        }
    }
}