﻿using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class StudentCourse
    {
        [Key]
        [Required]
        public Person Student { get; set; }
        [Key]
        [Required]
        public Course Course { get; set; }

        public int Score { get; set; }
    }
}