﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Person
    {
        [Key]
        public int PersonNumber { get; set; }
        
        public string Name { get; set; }
        public int Age { get; set; }

        //public ICollection<Address> Addresses { get; set; }
        [Required]
        public Address Address { get; set; }

        public ICollection<StudentCourse> Courses { get; set; }
    }
}