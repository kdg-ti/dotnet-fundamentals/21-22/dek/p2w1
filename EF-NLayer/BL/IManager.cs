﻿using System.Collections.Generic;
using Domain;

namespace BL
{
    public interface IManager
    {
        IEnumerable<Person> GetAllPeople(string name = null, int? age = null);
    }
}